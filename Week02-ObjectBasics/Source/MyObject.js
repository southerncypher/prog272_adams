var myObject = {

    numberOne :  1,
    numberTwo  : 2,
    word: 'myWord',

    functionOne : function(){
        'use strict';
        return 3;
    },

    functionTwo : function(){
        'use strict';
        return 'S';
    },

    functionThree : function(){
        'use strict';
        return true;
    }

};

