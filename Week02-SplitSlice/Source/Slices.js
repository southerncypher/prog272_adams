/**
 * Created by charliecalvert on 1/13/15.
 */

exports.stringSlices = {

    getFirstThreeCharacters: function(value) {
        'use strict';
        return value.slice(0, 3);
    },

    getLastThreeCharacters: function(value) {
        'use strict';
        return value.slice(-3);
    },

    getMiddleThreeCharacters : function (value) {
        'use strict';
        return value.slice((value.length/2) -1, (value.length /2) + 2);

    },

    getAllButFirstAndLast : function (value) {
        'use strict';
        return value.slice(1,-1)
    },

    getAllButFirstAndLastIfSame : function (value) {
        'use strict';
        var end = value.length -1;
        if(value[0] === value[end]) {
            return value.slice(1, -1);
        }

        else if(value[0] !== value[end]) {
            return value;
        }

    }



    
};
